package br.com.mastertech.itau.imersivo.objetos;

import br.com.mastertech.itau.imersivo.excecoes.TrianguloInvalidoException;

public class Triangulo extends Forma {
	
	private double ladoA;
	private double ladoB;
	private double ladoC;
	
	public Triangulo(double ladoA, double ladoB, double ladoC) {
		if((ladoA + ladoB) > ladoC && (ladoA + ladoC) > ladoB && (ladoB + ladoC) > ladoA) {
			this.ladoA = ladoA;
			this.ladoB = ladoB;
			this.ladoC = ladoC;
		}else {
			throw new TrianguloInvalidoException();
		}
	}
	
	@Override
	public String getNome() {
		return "Triangulo";
	}
	
	@Override
	public double calculaArea() {
		double s = (ladoA + ladoB + ladoC) / 2;       
		double area = Math.sqrt(s * (s - ladoA) * (s - ladoB) * (s - ladoC));
		return area;
	}
	
}
