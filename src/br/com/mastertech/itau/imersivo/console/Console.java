package br.com.mastertech.itau.imersivo.console;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import br.com.mastertech.itau.imersivo.objetos.Forma;

public class Console {
	
	private static Scanner scanner = new Scanner(System.in);
	
	public static List<Double> lerLados() {
		List<Double> lados = new ArrayList<>();
		
		boolean deveLerLado = true;
		
		while(deveLerLado) {
			imprimeMenu();
			
			Double lado = lerDouble();
			
			if(lado <= 0) {
				deveLerLado = false;
			}else {
				lados.add(lado);
			}
		}
		
		return lados;
	}
	
	public static double lerDouble() {
		String valor = scanner.nextLine();
		double numero = Double.parseDouble(valor);
		return numero;
	}
	
	public static void imprimeMenu() {
		System.out.println("Digite o valor do lado ou -1 para sair");
	}
	
	public static void imprimeArea(Forma forma) {
		System.out.println(forma);
	}

}
